
Work in progress. Nothing is done yet.

This is a breakout board for the Lambda PAF500F48, PAF600F48, PAF700F48 and possibly others. These are DC-DC 36-72 V to 12 V buck converters capable of 500, 600 and 700 W output with a stated 89% efficiency.

Input and output power connectors are XT60.

Everything is in KiCad format.